package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Set;

/**
 * Classe représentant les métadonnées du wallet
 */
public class DigitalWallet {

    /**
     * aggregations vers objet de DigitalBadges
     */
    private DigitalBadge badges;
    private DigitalBadge suppression;

    private Set<DigitalBadge> allBadges;
    private Set<Long> DeadLinesPositions;
    private Set<DigitalBadge> DeletingBadges;

    /**
     * constructeur par défaut
     */
    public DigitalWallet() {
    }

    public void addDeletingBadge(DigitalBadge delete) {
        // TODO: 22/12/2022  
    }

    /**
     * Setter pour allBadges
     * @return
     */
    public Set<DigitalBadge> getAllBadges() {
        return allBadges;
    }

    /**
     * Getter pour allBadges
     * @param allBadges
     */
    public void setAllBadges(Set<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }

    /**
     * Efface un badge du wallet
     * @param badge
     */
    public void commitBadgeDeletion(DigitalBadge badge) {

    }

    /**
     * Getter pour DeadLinesPositions
     * @return
     */
    public Set<Long> getDeadLinesPositions() {
        return DeadLinesPositions;
    }

    /**
     * Getter pour DeletingBadges
     * @return
     */
    public Set<DigitalBadge> getDeletingBadges() {
        return DeletingBadges;
    }

    /**
     * Setter pour DeadLinesPositions
     * @param deadLinesPositions
     */
    public void setDeadLinesPositions(Set<Long> deadLinesPositions) {
        DeadLinesPositions = deadLinesPositions;
    }

    public int size() {
        return this.size();
    }
}
