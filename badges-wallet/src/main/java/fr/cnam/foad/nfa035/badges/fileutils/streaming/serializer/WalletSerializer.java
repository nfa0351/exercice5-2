package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import java.io.IOException;

/**
 * interface définissant la serialisation du wallet
 * @param <S>
 * @param <M>
 */
public interface WalletSerializer<S, M> extends ImageStreamingSerializer<S,M> {

    /**
     * définition signature méthode annulation suppression
     * @param source
     * @param media
     * @throws IOException
     */
    void rollback(S source, M media) throws IOException;

}